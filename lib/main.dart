import 'package:flutter/material.dart';
import 'package:tuk_tuk_web/utils/app_router.dart';
import 'package:tuk_tuk_web/style_guide/constants.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  AppRouter.setup();
  runApp(MaterialApp(
      title: 'Tuk-Tuk',
      debugShowMaterialGrid: false,
      debugShowCheckedModeBanner: false,
      navigatorKey: NavigationService.navigatorKey,
      onGenerateRoute: AppRouter.router.generator,
      initialRoute: AppRouter.initial,
      theme: ThemeData(
          scaffoldBackgroundColor: kPrimaryDartGray,
          elevatedButtonTheme: ElevatedButtonThemeData(
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.black))),
          appBarTheme: const AppBarTheme(
              backgroundColor: kPrimaryDartGray, elevation: 0),
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.black),
          useMaterial3: true)));
}
