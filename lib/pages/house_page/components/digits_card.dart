import 'package:flutter/material.dart';
import 'package:tuk_tuk_web/style_guide/constants.dart';
import 'package:tuk_tuk_web/style_guide/sg_text.dart';

import '../../../utils/callbacks.dart';

class DigitsCard extends StatefulWidget {
  final StringCallback callback;

  const DigitsCard({super.key, required this.callback});

  @override
  State<DigitsCard> createState() => _DigitsCardState();
}

class _DigitsCardState extends State<DigitsCard> {
  String _total = "";

  _addDigit(String value) =>
      setState(() => widget.callback((_total += value).toString()));

  _removeLast() {
    if (_total.isNotEmpty) {
      _total = _total.substring(0, _total.length - 1);
    }
    setState(() => widget.callback(_total));
  }

  @override
  Widget build(BuildContext context) =>
      Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              NumKey(value: 1.toString(), callBack: _addDigit),
              NumKey(value: 2.toString(), callBack: _addDigit),
              NumKey(value: 3.toString(), callBack: _addDigit)
            ]),
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              NumKey(value: 4.toString(), callBack: _addDigit),
              NumKey(value: 5.toString(), callBack: _addDigit),
              NumKey(value: 6.toString(), callBack: _addDigit)
            ]),
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              NumKey(value: 7.toString(), callBack: _addDigit),
              NumKey(value: 8.toString(), callBack: _addDigit),
              NumKey(value: 9.toString(), callBack: _addDigit)
            ]),
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              NumKey(value: "#".toString(), callBack: _addDigit),
              NumKey(value: 0.toString(), callBack: _addDigit),

              Padding(
                  padding: const EdgeInsets.only(
                      left: kLowPadding, right: kLowPadding, top: kLowPadding),
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(kLowPadding)),
                          backgroundColor: kPrimaryLightGray,
                          elevation: 0,
                          maximumSize: const Size(200, 60),
                          minimumSize: const Size(110, 60)),
                      child: const Icon(Icons.backspace, color: kPrimaryBlack),
                      onPressed: () => _removeLast()))
            ])
      ]);
}

class NumKey extends StatelessWidget {
  final String value;
  Function callBack;

  NumKey({super.key, required this.value, required this.callBack});

  @override
  Widget build(BuildContext context) => Padding(
      padding: const EdgeInsets.only(
          left: kLowPadding, right: kLowPadding, top: kLowPadding),
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            foregroundColor: kPrimaryLightGray,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(kLowPadding)),
              backgroundColor: kPrimaryLightGray,
              elevation: 0,
              maximumSize: const Size(200, 60),
              minimumSize: const Size(110, 60)),
          child: SGText.appBar(value),
          onPressed: () => {callBack(value)}));
}
