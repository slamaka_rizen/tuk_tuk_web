import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:tuk_tuk_web/generated/assets.dart';
import 'package:tuk_tuk_web/pages/house_page/house_vm.dart';
import 'package:tuk_tuk_web/services/responses/resp_house/resp_house.dart';
import 'package:tuk_tuk_web/style_guide/constants.dart';
import 'package:tuk_tuk_web/style_guide/sg_button.dart';
import 'package:tuk_tuk_web/style_guide/sg_container.dart';
import 'package:tuk_tuk_web/style_guide/sg_text.dart';
import 'package:tuk_tuk_web/utils/app_router.dart';

import '../video_call_page/call_page.dart';
import 'components/digits_card.dart';

class HousePage extends StatefulWidget {
  final dynamic id;

  const HousePage({super.key, this.id});

  @override
  State<HousePage> createState() => _HousePageState();
}

class _HousePageState extends State<HousePage> {
  late final _vm = HouseVM(widget.id);

  String? _searchObject;

  @override
  void initState() {
    _vm.load();
    super.initState();

    checkPermission().then((value) => debugPrint("# checkPermission $value"));
  }

  Future checkPermission() async {
    final permission = await Permission.camera.status;
    final audio = await Permission.microphone.status;

    debugPrint("permission camera : $permission");
    debugPrint("audio : $audio");

    if (!permission.isGranted) {
      await Permission.camera.request().then((value) async {
        if (!audio.isGranted) {
          await Permission.microphone.request();
        }
      });
    } else {
      if (!audio.isGranted) {
        await Permission.microphone.request();
      }
    }

    return Future.value(true);
  }

  @override
  Widget build(BuildContext context) => Scaffold(
      body: StreamBuilder<RespHouse?>(
          stream: _vm.flats,
          builder: (context, snapshot) {
            var house = snapshot.data;
            if (house == null) return Center(child: SvgPicture.asset(Assets.imagesLoading));
            var flats = snapshot.data?.flats ?? [];
            if (flats.isEmpty) return Center(child: SvgPicture.asset(Assets.imagesEmpty));
            return SafeArea(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                  _buildDateAndNameCard(house),
                  _buildSearch(),
                  _buildFlatsCard(flats
                      .where((element) =>
                          element.value.contains(_searchObject ?? ""))
                      .toList()),
                  _buildNumberPadCard()
                ]));
          }));

  _buildFlatsCard(List<Flats> flats) =>flats.isEmpty ? Expanded(child: Center(child: SvgPicture.asset(Assets.imagesEmpty))) : Expanded(
      child: Padding(
          padding: const EdgeInsets.only(
              top: kLowPadding, left: kMediumPadding, right: kMediumPadding),
          child: ListView.builder(
              itemCount: flats.length,
              itemBuilder: (context, index) => Padding(
                  padding: const EdgeInsets.only(top: kLowPadding),
                  child: SGContainer(
                      backgroundColor: kPrimaryWhite,
                      child: Padding(
                          padding: const EdgeInsets.all(kMediumPadding),
                          child: Row(children: [
                            Expanded(child: SGText.regular(flats[index].value)),
                            IconButton(
                                icon: const Icon(Icons.call_outlined,
                                    color: kPrimaryBlack),
                                onPressed: () async {
                                  var respCall =
                                      await _vm.callToFlat(flats[index]);
                                  if (respCall != null && mounted) {
                                    KeyCenter.instance.zegoToken =
                                        respCall.zegoToken;
                                    KeyCenter.instance.roomID =
                                        respCall.id.toString();
                                    KeyCenter.instance.userID =
                                        respCall.zegoUserId.toString();

                                    Navigator.of(context)
                                        .pushNamed(AppRouter.video);
                                  }
                                })
                          ])))))));

  Padding _buildDateAndNameCard(RespHouse house) => Padding(
      padding: const EdgeInsets.all(kMediumPadding),
      child: Row(children: [
        IconButton(
            onPressed: () => Navigator.pop(context),
            icon: const Icon(Icons.arrow_back_ios_new, color: kPrimaryBlack)),
        Expanded(
            child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
              SGText.regular("${_formattedTime()} · ${_formattedDay()}",
                  color: kPrimaryBlack),
              SGText.appBar(house.name ?? " ")
            ]))
      ]));

  _formattedTime() => DateFormat('kk:mm').format(DateTime.now().toLocal());

  _formattedDay() => DateFormat('EEEE').format(DateTime.now().toLocal());

  _buildSearch() => Padding(
      padding:
          const EdgeInsets.only(left: kMediumPadding, right: kMediumPadding),
      child: SizedBox(
          height: 60,
          child: SGContainer(
              onPressed: () => setState(() => _searchObject = ""),
              backgroundColor: kPrimaryWhite,
              child: Padding(
                  padding: const EdgeInsets.all(kMediumPadding),
                  child: Row(children: [
                    const Icon(Icons.search, color: kPrimaryDartGray),
                    SGText.hint(_searchObject ?? "Наберите номер квартиры",
                        fontSize: kMediumPadding, color: kPrimaryDartGray)
                  ])))));

  _buildNumberPadCard() => _searchObject == null
      ? Container()
      : SGContainer(
          backgroundColor: kPrimaryWhite,
          child: Padding(
              padding: const EdgeInsets.all(kMediumPadding),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    DigitsCard(
                        callback: (text) =>
                            setState(() => _searchObject = text)),
                    const SizedBox(height: kMediumPadding),
                    SGButton.outline(
                        title: "Скрыть",
                        onPressed: () => setState(() => _searchObject = null))
                  ])));
}
