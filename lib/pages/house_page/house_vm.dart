import 'package:rxdart/rxdart.dart';
import 'package:tuk_tuk_web/services/responses/resp_house/resp_house.dart';
import 'package:tuk_tuk_web/style_guide/constants.dart';

import '../../services/network_service/house_service.dart';
import '../../services/responses/resp_call/resp_call.dart';

class HouseVM {
  final String _id;
  final HouseService _houseService = HouseService();
  final flats = BehaviorSubject<RespHouse?>.seeded(null);

  HouseVM(this._id);

  void load() async {
    var house = await _houseService.loadHouse(_id);
    flats.add(house);
  }

  Future<RespCall?> callToFlat(Flats flat) async {
    try {
      return await _houseService.callToFlat(flat.id);
    } catch (exception) {
      showScaffold(exception.toString());
      return null;
    }
  }
}
