import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:tuk_tuk_web/services/responses/resp_tuk_status/resp_tuk_status.dart';
import 'package:tuk_tuk_web/style_guide/constants.dart';
import 'package:tuk_tuk_web/style_guide/sg_text.dart';
import 'package:zego_express_engine/zego_express_engine.dart';
import 'call_vm.dart';

class KeyCenter {
  static final KeyCenter instance = KeyCenter._internal();

  KeyCenter._internal();

  int appID = 1102866375;

  String appSign =
      '662cc3c428e2869a0240dcfb717374c6e8d14d3c36c30ed4cbb7274e48dcd399';

  ZegoScenario scenario = ZegoScenario.StandardVideoCall;
  bool enablePlatformView = false;

  String roomID = "";
  String userID = "";
  String zegoToken = "";
}

class CallPage extends StatefulWidget {
  const CallPage({super.key});

  @override
  State<CallPage> createState() => _CallPageState();
}

class _CallPageState extends State<CallPage> {
  final CallVM _viewModel = CallVM(KeyCenter.instance.roomID);

  Widget? localView;
  Widget? remoteView;

  int? localViewID;
  int? remoteViewID;

  String getRoomId() => KeyCenter.instance.roomID;

  String getUserId() => KeyCenter.instance.userID;

  @override
  void initState() {
    super.initState();
    _viewModel.viewDidLoad((status) {
      Navigator.pop(context);
      _showAlert(status);
    });
    _startListenEvent();
    _loginRoom(getUserId(), getUserId());
  }

  _showAlert(TukStatus status) async {
    showModalBottomSheet(
        context: context,
        builder: (context) => SafeArea(
            child: Padding(
                padding: const EdgeInsets.all(kMediumPadding),
                child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      SGText.appBar(status.title),
                      const SizedBox(height: kMediumPadding),
                      SGText.regular(status.description),
                      const SizedBox(height: kHeadlinePadding)
                    ]))));
    await Future.delayed(const Duration(seconds: 3));
    if (mounted) Navigator.pop(context);
  }

  @override
  void dispose() {
    _logoutRoom();
    _viewModel.deInit();
    super.dispose();
  }

  String _roomId = '';

  void _startListenEvent() {
    ZegoExpressEngine.onExceptionOccurred = (ZegoScreenCaptureSource source,
        ZegoScreenCaptureSourceExceptionType exceptionType) {
      debugPrint("🎁 onExceptionOccurred $source $exceptionType");
    };
    ZegoExpressEngine.onPublisherStateUpdate = (String streamID,
        ZegoPublisherState state,
        int errorCode,
        Map<String, dynamic> extendedData) {
      debugPrint(
          '🎁 onPublisherStateUpdate $state errorCode $errorCode, extendedData $extendedData');
    };

    ZegoExpressEngine.onRoomStreamUpdate =
        (roomID, updateType, List<ZegoStream> streamList, extendedData) {
      debugPrint(
          '🎁 onRoomStreamUpdate: roomID: $roomID, updateType: $updateType, '
          'streamList: ${streamList.map((e) => e.streamID)}, extendedData: $extendedData');
      if (updateType == ZegoUpdateType.Add) {
        for (final stream in streamList) {
          _startPlayStream(stream.streamID);
        }
      } else {
        for (final stream in streamList) {
          _stopPlayStream(stream.streamID);
        }
      }
    };

    ZegoExpressEngine.onDebugError =
        (int errorCode, String funcName, String info) {
      debugPrint("🎁 onDebugError $errorCode $funcName $info");
    };
  }

  Future<void> _startPlayStream(String streamID) async {
    var view = await ZegoExpressEngine.instance.createCanvasView((viewID) {
      remoteViewID = viewID;
      ZegoCanvas canvas = ZegoCanvas(viewID, viewMode: ZegoViewMode.AspectFill);
      ZegoExpressEngine.instance.startPlayingStream(streamID, canvas: canvas);
    });
    setState(() => remoteView = view);
  }

  Future<void> _stopPlayStream(String streamID) async {
    ZegoExpressEngine.instance.stopPlayingStream(streamID);
    if (remoteViewID != null) {
      ZegoExpressEngine.instance.destroyCanvasView(remoteViewID!);
      setState(() {
        remoteViewID = null;
        remoteView = null;
      });
    }
  }

  Future<ZegoRoomLoginResult> _loginRoom(String userID, String userNm) async {
    var profile = ZegoEngineProfile(
      KeyCenter.instance.appID,
      ZegoScenario.Default,
      appSign: KeyCenter.instance.appSign,
    );
    if (kIsWeb) {
      profile.appSign = null;
    }
    await ZegoExpressEngine.createEngineWithProfile(profile);

    final user = ZegoUser(userID, userNm);

    _roomId = getRoomId();

    ZegoRoomConfig roomConfig = ZegoRoomConfig.defaultConfig()
      ..token = KeyCenter.instance.zegoToken
      ..isUserStatusNotify = true;

    var loginRoomResult = await ZegoExpressEngine.instance
        .loginRoom(getRoomId(), user, config: roomConfig);

    debugPrint('loginRoom: errorCode:${loginRoomResult.errorCode}, '
        'extendedData:${loginRoomResult.extendedData}');
    if (loginRoomResult.errorCode == 0) {
      _startPreview();
      _startPublish();
    } else if (mounted) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text('loginRoom failed: ${loginRoomResult.errorCode}')));
    }

    return loginRoomResult;
  }

  Future<dynamic> _logoutRoom() async {
    stopPreview();
    stopPublish();
    await ZegoExpressEngine.instance.logoutRoom(_roomId);
    await ZegoExpressEngine.destroyEngine();
  }

  ///Start and stop preview
  Future<void> _startPreview() async {
    localView =
        await ZegoExpressEngine.instance.createCanvasView((viewID) async {
      localViewID = viewID;
      ZegoCanvas previewCanvas =
          ZegoCanvas(viewID, viewMode: ZegoViewMode.AspectFill);
      await ZegoExpressEngine.instance.startPreview(canvas: previewCanvas);
    });
    setState(() {});
  }

  Future<void> stopPreview() async {
    ZegoExpressEngine.instance.stopPreview();
    if (localViewID != null) {
      await ZegoExpressEngine.instance.destroyCanvasView(localViewID!);
      setState(() {
        localViewID = null;
        localView = null;
      });
    }
  }

  ///Start publish and stop publish
  Future<void> _startPublish() async {
    try {
      String streamID = getUserId();
      await ZegoExpressEngine.instance.enableCamera(true);
      await ZegoExpressEngine.instance.useFrontCamera(true);
      await ZegoExpressEngine.instance
          .startPublishingStream("${streamID}_main");
    } catch (exception) {
      debugPrint(exception.toString());
    }
  }

  Future<void> stopPublish() async {
    return ZegoExpressEngine.instance.stopPublishingStream();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(elevation: 0),
      body: Stack(children: [
        if (localView != null) localView!,
        if (remoteView != null)
          Positioned(
              height: 64,
              width: 64,
              top: kMediumPadding,
              right: kMediumPadding,
              child: Container(
                  clipBehavior: Clip.antiAlias,
                  decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.circular(kMediumPadding)),
                  child: SizedBox(
                      width: MediaQuery.of(context).size.width / 3,
                      child: AspectRatio(
                          aspectRatio: 9.0 / 16.0, child: remoteView)))),
        Positioned(
            bottom: 12,
            left: 0,
            right: 0,
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    shape: const CircleBorder(), backgroundColor: Colors.red),
                onPressed: () async {
                  await _logoutRoom();
                  if (mounted) {
                    Navigator.pop(context);
                  }
                },
                child: const Center(
                    child: Padding(
                        padding: EdgeInsets.all(kMediumPadding),
                        child: Icon(Icons.call_end,
                            size: 32, color: kPrimaryWhite)))))
      ]));
}
