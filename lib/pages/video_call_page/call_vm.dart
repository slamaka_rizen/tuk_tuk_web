import 'dart:async';
import 'dart:ui';

import 'package:tuk_tuk_web/services/network_service/house_service.dart';
import 'package:tuk_tuk_web/services/responses/resp_tuk_status/resp_tuk_status.dart';

typedef StatusCallback = Function(TukStatus);

class CallVM {
  final String _callID;
  final HouseService _houseService = HouseService();

  CallVM(this._callID);

  late final StreamSubscription<dynamic>? _streamSubscription;

  viewDidLoad(StatusCallback backToHousePage) async {
    _streamSubscription =
        Stream.periodic(const Duration(seconds: 4), (attempt) async {
      var response = await _houseService.checkStatus(_callID);
      switch (response.statusType) {
        case TukStatus.declined:
          backToHousePage(response.statusType);
          break;
        case TukStatus.pending:
          break;
        case TukStatus.accepted:
          backToHousePage(response.statusType);
          break;
      }
    }).listen((event) => print);
  }

  deInit() => _streamSubscription?.cancel();
}
