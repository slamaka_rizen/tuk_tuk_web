import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:tuk_tuk_web/generated/assets.dart';
import 'package:tuk_tuk_web/style_guide/constants.dart';
import 'package:tuk_tuk_web/style_guide/sg_container.dart';
import 'package:tuk_tuk_web/style_guide/sg_text.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({super.key});

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(
          centerTitle: false,
          title: Row(children: [
            SizedBox(
                height: 120,
                width: 60,
                child: Lottie.asset(Assets.animationsHouseAnimation)),
            Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: SGText.headline("Tuk-Tuk"))
          ])),
      body: Center(
          child: SGContainer(
              backgroundColor: kPrimaryWhite,
              child: Padding(
                  padding: const EdgeInsets.all(kHeadlinePadding),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SGText.headline("Welcome"),
                        SGText.regular("To Our Company"),
                        SGText.hint("It Компания для ваших удобств ")
                      ])))));
}
