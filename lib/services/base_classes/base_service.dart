import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart';

class BaseService {
  String? token;

  String baseURL = "https://gettuktuk.com"; // production
  // String baseURL = "https://devapi.gettuktuk.com"; // production dev
// String baseURL = "http://gettuktuk.com:3001"; // production dev(release)


  bool checkSuccess(Response response) =>
      response.statusCode >= 200 && response.statusCode <= 299;

  Map<String, String> get headers => {
        HttpHeaders.acceptHeader: "application/json",
        HttpHeaders.contentTypeHeader: 'application/json',
        if (token != null) HttpHeaders.authorizationHeader: "Bearer $token"
      };

  Never extractError(Response response) => throw (Exception(
      [json.decode(response.body)['message'] ?? response.reasonPhrase]));

  printResponse({required Response response}) {
    debugPrint('--------------- Response ----------------');
    debugPrint('# Remote address: ${response.request?.url}');
    debugPrint('# Methode: ${response.request?.method}');
    debugPrint('# Headers: ${response.request?.headers['authorization']}');
    debugPrint('# Status: ${response.statusCode}');
    debugPrint('# Body: ${response.body}');

    debugPrint('--------------- Response ----------------');
  }
}
