import 'dart:convert';
import 'package:http/http.dart';
import 'package:tuk_tuk_web/services/base_classes/base_service.dart';
import 'package:tuk_tuk_web/services/responses/resp_house/resp_house.dart';

import '../responses/resp_call/resp_call.dart';
import '../responses/resp_tuk_status/resp_tuk_status.dart';

class HouseService extends BaseService {
  Future<RespHouse> loadHouse(String qrCode) async {
    var response = await get(Uri.parse('$baseURL/houses/$qrCode'));
    printResponse(response: response);
    if (checkSuccess(response)) {
      return RespHouse.fromJson(json.decode(utf8.decode(response.bodyBytes)));
    } else {
      extractError(response);
    }
  }

  Future<RespCall> callToFlat(dynamic flatID) async {
    var response = await post(Uri.parse('$baseURL/tuktuk'),
        headers: headers, body: json.encode({"flat_id": flatID}));
    printResponse(response: response);
    if (checkSuccess(response)) {
      return RespCall.fromJson(json.decode(utf8.decode(response.bodyBytes)));
    } else {
      extractError(response);
    }
  }

  Future<RespTukStatus> checkStatus(dynamic tukTukID) async {
    var response =
        await get(Uri.parse('$baseURL/tuktuk/$tukTukID'), headers: headers);
    printResponse(response: response);
    if (checkSuccess(response)) {
      return RespTukStatus.fromJson(json.decode(utf8.decode(response.bodyBytes)));
    } else {
      extractError(response);
    }
  }
}
