import 'dart:convert';

/// status : "PENDING"
/// id : 7136
/// date : "2024-03-04T14:12:53.000Z"

RespCall respCallFromJson(String str) => RespCall.fromJson(json.decode(str));

String respCallToJson(RespCall data) => json.encode(data.toJson());

class RespCall {
  RespCall({this.id, required this.status, required this.date});

  RespCall.fromJson(dynamic json) {
    id = json['id'];
    date = json['date'];
    status = json['status'];
    zegoToken = json['zego_token'];
    zegoUserId = json['zego_user_id'];
  }

  dynamic id = 0;
  String status = "";
  String date = "";
  String zegoToken = "";
  String zegoUserId = "";

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = status;
    map['id'] = id;
    map['date'] = date;
    return map;
  }
}
