import 'dart:convert';

/// id : 135
/// name : "BIG BOY 9"
/// bluetooth : "wbbrstizam|:id135"
/// status : "normal"
/// alert : null
/// distance : 100
/// distance2 : 20
/// directorPhone : null
/// serviceUuid : "974f7641-53b8-4c3e-9e55-dab34461d08f"
/// characteristicUuidReceive : "7a61c7b6-fc91-453e-9131-4b391213c9b0"
/// characteristicUuidNotify : "b1e83d7c-570f-44a9-a63d-05659cb13f8c"
/// aesKey : "haveybtmnxkvknzhkiunqnrkudzhyihy"
/// salt : "wtvhxuajdgtgydqanxxnfv"
/// espPhone : "+77756043100"
/// whiteList : null
/// is_mqtt_available : true
/// is_alert_available : false
/// flats : [{"id":4897,"value":"1","leader":null},{"id":4898,"value":"2","leader":null}]
/// houseType : {"id":1,"name":"house"}
/// cameras : [{"id":1,"name":"Тестовая камера для стрима видео потока","url":"https://cph-p2p-msl.akamaized.net/hls/live/2000341/test/master.m3u8","houses":[{"id":107,"name":"id:107 bee mqtt","bluetooth":"huilmxljsn|:id107","status":"normal","alert":null,"distance":100,"distance2":20,"directorPhone":null,"serviceUuid":"974f7641-53b8-4c3e-9e55-dab34461d08f","characteristicUuidReceive":"7a61c7b6-fc91-453e-9131-4b391213c9b0","characteristicUuidNotify":"b1e83d7c-570f-44a9-a63d-05659cb13f8c","aesKey":"evmqetucahadajwchifpmxeoevjgvybr","salt":"bkuakijbspvkzplqfbvkuv","espPhone":"+77056772793","whiteList":null,"is_mqtt_available":true,"is_alert_available":true},{"id":135,"name":"BIG BOY 9","bluetooth":"wbbrstizam|:id135","status":"normal","alert":null,"distance":100,"distance2":20,"directorPhone":null,"serviceUuid":"974f7641-53b8-4c3e-9e55-dab34461d08f","characteristicUuidReceive":"7a61c7b6-fc91-453e-9131-4b391213c9b0","characteristicUuidNotify":"b1e83d7c-570f-44a9-a63d-05659cb13f8c","aesKey":"hbveyaimnxkvknzkkiunhnrhudzhytqy","salt":"wtnhxuajdgtgvdqanxxvfy","espPhone":"+77756043100","whiteList":null,"is_mqtt_available":true,"is_alert_available":false},{"id":210,"name":"AH 47","bluetooth":"mjxyqxwhcw|:id210","status":"normal","alert":null,"distance":100,"distance2":20,"directorPhone":null,"serviceUuid":"974f7641-53b8-4c3e-9e55-dab34461d08f","characteristicUuidReceive":"7a61c7b6-fc91-453e-9131-4b391213c9b0","characteristicUuidNotify":"b1e83d7c-570f-44a9-a63d-05659cb13f8c","aesKey":"wucgxfnektpqfnufbbcxmhbkuhrdqdbs","salt":"novyrfrqepchjfmlhxwglr","espPhone":"+77713218467","whiteList":[],"is_mqtt_available":false,"is_alert_available":true}]}]
/// moderators : []

RespHouse respHouseFromJson(String str) => RespHouse.fromJson(json.decode(str));

String respHouseToJson(RespHouse data) => json.encode(data.toJson());

class RespHouse {
  RespHouse({this.id, this.name, this.status, this.flats, this.houseType});

  RespHouse.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    status = json['status'];
    if (json['flats'] != null) {
      flats = [];
      json['flats'].forEach((v) {
        flats?.add(Flats.fromJson(v));
      });
    }
    houseType = json['houseType'] != null
        ? HouseType.fromJson(json['houseType'])
        : null;
  }

  num? id;
  String? name;
  String? status;
  List<Flats>? flats;
  HouseType? houseType;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['status'] = status;
    if (flats != null) {
      map['flats'] = flats?.map((v) => v.toJson()).toList();
    }
    if (houseType != null) {
      map['houseType'] = houseType?.toJson();
    }

    return map;
  }
}

/// id : 1
/// name : "house"

HouseType houseTypeFromJson(String str) => HouseType.fromJson(json.decode(str));

String houseTypeToJson(HouseType data) => json.encode(data.toJson());

class HouseType {
  HouseType({
    this.id,
    this.name,
  });

  HouseType.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
  }

  num? id;
  String? name;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    return map;
  }
}

/// id : 4897
/// value : "1"
/// leader : null

Flats flatsFromJson(String str) => Flats.fromJson(json.decode(str));

String flatsToJson(Flats data) => json.encode(data.toJson());

class Flats {
  Flats({this.id, this.leader, required this.value});

  Flats.fromJson(dynamic json) {
    id = json['id'];
    value = json['value'];
    leader = json['leader'];
  }

  dynamic id;
  String value = "";
  dynamic leader;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['value'] = value;
    map['leader'] = leader;
    return map;
  }
}
