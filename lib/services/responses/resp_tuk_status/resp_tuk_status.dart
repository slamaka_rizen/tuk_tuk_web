
import 'package:collection/collection.dart';

/// id : 7510
/// date : "2024-03-22T16:01:46.000Z"
/// status : "PENDING"

enum TukStatus { declined, pending, accepted }

extension TukStatusOn on TukStatus {
  String get title {
    switch (this) {
      case TukStatus.declined:
        return "😡 Упс...";
      case TukStatus.pending:
        return "🏃‍ В процессе";
      case TukStatus.accepted:
        return "✅ Успешно!";
    }
  }

  String get description {
    switch (this) {
      case TukStatus.declined:
        return "Вам отказали в доступе ❌";
      case TukStatus.pending:
        return "Еще не ответили на Ваш запрос 🕒";
      case TukStatus.accepted:
        return "Дверь скоро откроется! 🚪";
    }
  }
}

class RespTukStatus {
  RespTukStatus({
    this.id,
    this.date,
    this.status,
  });

  RespTukStatus.fromJson(dynamic json) {
    id = json['id'];
    date = json['date'];
    status = json['status'];
  }

  num? id;
  String? date;
  String? status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['date'] = date;
    map['status'] = status;
    return map;
  }

  TukStatus get statusType =>
      TukStatus.values.firstWhereOrNull(
          (e) => e.name.toLowerCase().contains(status?.toLowerCase() ?? " ")) ??
      TukStatus.pending;
}
