import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:tuk_tuk_web/style_guide/sg_text.dart';

import 'package:google_fonts/google_fonts.dart';

TextStyle kTextStyleRegular = GoogleFonts.lato(
    fontWeight: FontWeight.w400, fontSize: 16, color: kPrimaryBlack);
TextStyle kTextStyleHint = GoogleFonts.lato(
    fontWeight: FontWeight.w400, fontSize: 14, color: kPrimaryDartGray);

TextStyle kTextStyleHeading = GoogleFonts.lato(
    fontWeight: FontWeight.w500, fontSize: 16, color: kPrimaryBlack);

TextStyle kTextStyleAppBarTitle = GoogleFonts.lato(
    fontWeight: FontWeight.bold, fontSize: 18, color: kPrimaryBlack);

const Color kPrimaryBlack = Color(0xff0F0F0F);
const Color kPrimaryWhite = Color(0xffFAFAFA);
Color kPrimaryWhite70 = const Color(0xffffffff).withOpacity(0.7);
const Color kPrimaryDartGray = Color(0xffD6DBE1);
const Color kPrimaryLightGray = Color(0xffF1F5F9);
const Color kPrimaryGreen = Color(0xff05944F);
const Color kPrimaryRed = Color(0xffe83039);

const double kHeadlinePadding = 24.0;
const double kMediumPadding = 16.0;
const double kLowPadding = 8.0;

List<BoxShadow>? kPrimaryShadow = [
  const BoxShadow(
      offset: Offset(0.0, 1.0), blurRadius: 13, color: Color(0x0F000000)),
];

BoxDecoration kContainerDecoration = BoxDecoration(
    color: kPrimaryWhite,
    boxShadow: kPrimaryShadow,
    borderRadius: BorderRadius.circular(kLowPadding));

BoxDecoration kContainerWhiteDecoration = BoxDecoration(
    color: kPrimaryWhite, borderRadius: BorderRadius.circular(15));

BoxDecoration kContainerDecorationTopRound = BoxDecoration(
    color: kPrimaryWhite,
    boxShadow: kPrimaryShadow,
    borderRadius: const BorderRadius.only(
        topRight: Radius.circular(15), topLeft: Radius.circular(15)));

var kRoundedRectangleBorder =
    RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0));

bool isMobile = kIsWeb;

showScaffold(String message, {BuildContext? context}) => ScaffoldMessenger.of(
        context ?? NavigationService.navigatorKey.currentContext!)
    .showSnackBar(
        SnackBar(content: SGText.regular(message, color: Colors.white)));

class NavigationService {
  static GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
}
