import 'package:flutter/material.dart';

import 'constants.dart';

class SGButton extends StatelessWidget {
  final bool _outline;
  final bool _textButton;
  final String title;
  final bool disabled;
  final Widget? leading;
  final VoidCallback onPressed;
  final double height;
  final Color? textColor;

  const SGButton.elevated(
      {Key? key,
      this.leading,
      this.height = 48,
      required this.title,
      this.disabled = false,
      required this.onPressed, this.textColor})
      : _outline = false,
        _textButton = false,
        super(key: key);

  const SGButton.outline(
      {super.key,
      this.leading,
      this.height = 48,
      required this.title,
      required this.onPressed, this.textColor})
      : disabled = false,
        _textButton = false,
        _outline = true;

  const SGButton.text(
      {super.key,
      this.leading,
      this.height = 48,
      required this.title,
      required this.onPressed, this.textColor})
      : disabled = false,
        _textButton = true,
        _outline = false;

  @override
  Widget build(BuildContext context) {
    return _textButton
        ? _buildTextButton()
        : _outline
            ? _buildOutlinedButton()
            : _buildElevatedButton();
  }

  TextButton _buildTextButton() {
    var textWidget =
        Text(title, style: kTextStyleHeading.copyWith(fontSize: 15, color: textColor ?? kTextStyleHeading.color));

    return TextButton(
        onPressed: onPressed,
        child: leading == null
            ? textWidget
            : Row(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [leading!, const SizedBox(width: 12), textWidget]));
  }

  _buildElevatedButton() {
    var textWidget = Text(title,
        style: kTextStyleRegular.copyWith(
            fontWeight: FontWeight.w500, color: Colors.white));
    return ConstrainedBox(
      constraints: BoxConstraints.tightFor(height: height),
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(backgroundColor: kPrimaryBlack),
          onPressed: onPressed,
          child: leading == null
              ? textWidget
              : Row(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [leading!, const SizedBox(width: 12), textWidget])),
    );
  }

  _buildOutlinedButton() {
    var textWidget = Text(title,
        style: kTextStyleRegular.copyWith(
            fontWeight: FontWeight.bold, color: textColor ?? kTextStyleRegular.color));
    return ConstrainedBox(
      constraints: BoxConstraints.tightFor(height: height),
      child: OutlinedButton(
          onPressed: onPressed,
          style: OutlinedButton.styleFrom(
              foregroundColor: kPrimaryBlack,
              side: const BorderSide(color: kPrimaryDartGray, width: 1)),
          child: leading == null
              ? textWidget
              : Row(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [leading!, const SizedBox(width: 12), textWidget])),
    );
  }
}
