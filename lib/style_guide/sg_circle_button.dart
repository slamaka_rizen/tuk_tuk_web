import 'package:flutter/material.dart';

import 'constants.dart';

class SGCircleButton extends StatelessWidget {
  final Widget icon;
  final VoidCallback callback;
  final Color background;

  const SGCircleButton(
      {Key? key,
      required this.icon,
      required this.callback,
      this.background = Colors.transparent})
      : super(key: key);

  @override
  Widget build(BuildContext context) => InkWell(
      onTap: () => callback(),
      child: Container(
          decoration: BoxDecoration(shape: BoxShape.circle, color: background),
          padding: const EdgeInsets.all(kLowPadding),
          child: icon));
}
