import 'package:flutter/material.dart';

import 'constants.dart';

class SGContainer extends StatelessWidget {
  final Widget child;
  final VoidCallback? onPressed;
  final BoxDecoration decoration;

  SGContainer(
      {Key? key,
      this.onPressed,
      required this.child,
      BoxDecoration? decoration,
      Color backgroundColor = kPrimaryLightGray})
      : decoration =
            decoration ?? kContainerDecoration.copyWith(color: backgroundColor),
        super(key: key);

  @override
  Widget build(BuildContext context) => onPressed == null
      ? Container(decoration: decoration, child: child)
      : Ink(
          decoration: decoration,
          child: InkWell(
              borderRadius: const BorderRadius.all(Radius.circular(kLowPadding / 2)),
              onTap: () => onPressed?.call(),
              child: child));
}
