import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lottie/lottie.dart';
import 'package:tuk_tuk_web/style_guide/sg_text.dart';
import '../generated/assets.dart';
import 'constants.dart';

class SGEmpty extends StatelessWidget {
  const SGEmpty({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Padding(
      padding: const EdgeInsets.all(kHeadlinePadding),
      child: Center(
          child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
            if (isMobile)
              SizedBox(
                  height: 200,
                  child: LottieBuilder.network(
                      "https://assets4.lottiefiles.com/packages/lf20_BvR4Heww3W.json"))
            else
              SizedBox(
                  width: 200,
                  height: 200,
                  child: SvgPicture.asset(Assets.imagesEmpty)),
            const SizedBox(height: kHeadlinePadding),
            SGText.headline("Пусто...")
          ])));
}
