import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../generated/assets.dart';

class SGImage extends StatelessWidget {
  final String? url;

  const SGImage({super.key, this.url});

  @override
  Widget build(BuildContext context) => (url == null || url == "")
      ? SvgPicture.asset(Assets.imagesEmpty)
      : CachedNetworkImage(
          imageUrl: url!,
          width: MediaQuery.sizeOf(context).width,
          fit: BoxFit.fitWidth,
          errorWidget: (_, a, b) =>
              SvgPicture.asset(Assets.imagesEmpty));
}
