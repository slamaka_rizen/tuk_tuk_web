import 'package:flutter/material.dart';
import 'dart:async';
import 'constants.dart';

class SGLongPressButton extends StatefulWidget {
  final Widget icon;
  final Color background;
  final VoidCallback callback;

  const SGLongPressButton(
      {Key? key,
      required this.icon,
      required this.callback,
      this.background = Colors.transparent})
      : super(key: key);

  @override
  State<SGLongPressButton> createState() => _SGLongPressButtonState();
}

class _SGLongPressButtonState extends State<SGLongPressButton> {
  Timer? _timer;

  @override
  Widget build(BuildContext context) => InkWell(
      onTapDown: (_) => _startTimer(),
      onTap: () => widget.callback(),
      onTapUp: (_) => _stopTimer(),
      child: Container(
          decoration:
              BoxDecoration(shape: BoxShape.circle, color: widget.background),
          padding: const EdgeInsets.all(kLowPadding),
          child: widget.icon));

  void _startTimer() {
    _timer = Timer.periodic(
        const Duration(milliseconds: 100), (timer) => widget.callback());
  }

  void _stopTimer() => _timer?.cancel();

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }
}
