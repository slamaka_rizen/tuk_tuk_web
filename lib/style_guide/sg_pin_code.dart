import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import 'constants.dart';

class SGPinCode extends StatelessWidget {
  final ValueChanged<String> onComplete;

  const SGPinCode({Key? key, required this.onComplete}) : super(key: key);

  @override
  Widget build(BuildContext context) => PinCodeTextField(
      keyboardType: TextInputType.number,
      scrollPadding: EdgeInsets.zero,
      textStyle: kTextStyleHeading,
      length: 6,
      pinTheme: PinTheme(
          fieldOuterPadding: EdgeInsets.zero,
          shape: PinCodeFieldShape.box,
          fieldHeight: 50,
          fieldWidth: 40,
          borderRadius: BorderRadius.circular(kLowPadding),
          activeFillColor: kPrimaryWhite,
          inactiveFillColor: kPrimaryWhite,
          selectedFillColor: kPrimaryWhite,
          activeColor: kPrimaryWhite,
          inactiveColor: kPrimaryWhite,
          selectedColor: kPrimaryWhite),
      animationDuration: const Duration(milliseconds: 100),
      enableActiveFill: true,
      onCompleted: onComplete,
      onChanged: (value) {},
      appContext: context);
}
