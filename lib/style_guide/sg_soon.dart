import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:tuk_tuk_web/style_guide/sg_text.dart';
import 'constants.dart';

class SGSoon extends StatelessWidget {
  const SGSoon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Padding(
      padding: const EdgeInsets.all(kHeadlinePadding),
      child: Center(
          child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
            if (isMobile)
              LottieBuilder.network(
                  "https://assets10.lottiefiles.com/packages/lf20_DTosIIqiu8.json"),
            SGText.headline("Мы в процессе разработки, еще чуть чуть...")
          ])));
}
