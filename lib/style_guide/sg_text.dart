import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'constants.dart';

class SGText extends StatelessWidget {
  final String text;
  final TextStyle style;
  final TextAlign textAlign;
  final TextStyle? highlightingStyle;
  final List<String> textHighlighting;

  final String? copyableText;

  SGText.appBar(this.text,
      {super.key,
      double fontSize = 24,
      color = kPrimaryBlack,
      this.textAlign = TextAlign.left,
      this.textHighlighting = const [],
      this.highlightingStyle,
      this.copyableText})
      : style =
            kTextStyleAppBarTitle.copyWith(color: color, fontSize: fontSize);

  SGText.headline(this.text,
      {super.key,
      double fontSize = 24,
      color = kPrimaryBlack,
      this.textAlign = TextAlign.left,
      this.textHighlighting = const [],
      this.highlightingStyle,
      this.copyableText})
      : style = kTextStyleHeading.copyWith(color: color, fontSize: fontSize);

  SGText.regular(this.text,
      {super.key,
      double fontSize = 16,
      color = kPrimaryBlack,
      this.textAlign = TextAlign.left,
      this.textHighlighting = const [],
      this.highlightingStyle,
      this.copyableText})
      : style = kTextStyleRegular.copyWith(color: color, fontSize: fontSize);

  SGText.bold(this.text,
      {super.key,
      double fontSize = 16,
      color = kPrimaryBlack,
      this.textAlign = TextAlign.left,
      this.textHighlighting = const [],
      this.highlightingStyle,
      this.copyableText})
      : style = kTextStyleRegular.copyWith(
            color: color, fontSize: fontSize, fontWeight: FontWeight.bold);

  SGText.hint(this.text,
      {super.key,
      color = kPrimaryDartGray,
      double fontSize = 13,
      this.textAlign = TextAlign.left,
      this.textHighlighting = const [],
      this.highlightingStyle,
      this.copyableText})
      : style = kTextStyleHint.copyWith(color: color, fontSize: fontSize);

  @override
  Widget build(BuildContext context) => GestureDetector(
      onTap: copyableText != null
          ? () {
              if (copyableText != null) _copy(context);
            }
          : null,
      child: textHighlighting.isNotEmpty
          ? SGHighlightText(
              text: text,
              style: style,
              textAlign: textAlign,
              highlights: textHighlighting,
              highlightingStyle: highlightingStyle ??
                  style.copyWith(
                      fontWeight: FontWeight.bold, color: kPrimaryRed))
          : Text(text, style: style, textAlign: textAlign));

  _copy(BuildContext context) =>
      Clipboard.setData(ClipboardData(text: copyableText ?? ""))
          .then((_) => showScaffold('Скопирован: $copyableText'));
}

class SGHighlightText extends StatelessWidget {
  final String text;
  final TextStyle style;
  final bool caseSensitive;
  final TextAlign textAlign;
  final List<String> highlights;
  final TextStyle highlightingStyle;

  const SGHighlightText(
      {super.key,
      required this.text,
      required this.style,
      required this.highlights,
      this.caseSensitive = true,
      required this.highlightingStyle,
      this.textAlign = TextAlign.justify});

  @override
  Widget build(BuildContext context) {
    if (text == '') {
      return _richText(_normalSpan(text));
    }
    if (highlights.isEmpty) {
      return _richText(_normalSpan(text));
    }
    for (int i = 0; i < highlights.length; i++) {
      if (highlights[i].isEmpty) {
        assert(highlights[i].isNotEmpty);
        return _richText(_normalSpan(text));
      }
    }

    List<TextSpan> spans = [];
    int start = 0;

    String lowerCaseText = text.toLowerCase();
    List<String> lowerCaseHighlights = [];

    for (var element in highlights) {
      lowerCaseHighlights.add(element.toLowerCase());
    }

    while (true) {
      Map<int, String> highlightsMap = {}; //key (index), value (highlight).

      if (caseSensitive) {
        for (int i = 0; i < highlights.length; i++) {
          int index = text.indexOf(highlights[i], start);
          if (index >= 0) {
            highlightsMap.putIfAbsent(index, () => highlights[i]);
          }
        }
      } else {
        for (int i = 0; i < highlights.length; i++) {
          int index = lowerCaseText.indexOf(lowerCaseHighlights[i], start);
          if (index >= 0) {
            highlightsMap.putIfAbsent(index, () => highlights[i]);
          }
        }
      }

      if (highlightsMap.isNotEmpty) {
        List<int> indexes = [];
        highlightsMap.forEach((key, value) => indexes.add(key));

        int currentIndex = indexes.reduce(min);
        String currentHighlight = text.substring(currentIndex,
            currentIndex + (highlightsMap[currentIndex]?.length ?? 0));

        if (currentIndex == start) {
          spans.add(_highlightSpan(currentHighlight));
          start += currentHighlight.length;
        } else {
          spans.add(_normalSpan(text.substring(start, currentIndex)));
          spans.add(_highlightSpan(currentHighlight));
          start = currentIndex + currentHighlight.length;
        }
      } else {
        spans.add(_normalSpan(text.substring(start, text.length)));
        break;
      }
    }

    return _richText(TextSpan(children: spans));
  }

  TextSpan _highlightSpan(String value) =>
      TextSpan(text: value, style: highlightingStyle);

  TextSpan _normalSpan(String value) => TextSpan(text: value, style: style);

  RichText _richText(TextSpan text) =>
      RichText(key: key, text: text, textAlign: textAlign);
}
