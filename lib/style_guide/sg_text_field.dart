import 'package:flutter/material.dart';

import 'constants.dart';

class SGTextField extends StatelessWidget {
  final TextEditingController? controller;
  final String? placeholder;
  final Widget? leading;
  final Widget? trailing;
  final bool password;
  final int minLines;
  final int maxLines;
  final bool autofocus;
  final String? initialValue;
  final TextInputType? keyboardType;
  final TextInputAction? textInputAction;
  final void Function()? trailingTapped;
  final ValueChanged<String>? onChanged;

  final circularBorder =
      OutlineInputBorder(borderRadius: BorderRadius.circular(8));

  SGTextField(
      {Key? key,
      this.controller,
      this.placeholder,
      this.leading,
      this.trailing,
      this.trailingTapped,
      this.password = false,
      this.initialValue,
      this.onChanged,
      this.keyboardType,
      this.autofocus = true,
      this.maxLines = 1,
      this.minLines = 1,
      this.textInputAction})
      : super(key: key);

  @override
  Widget build(BuildContext context) => Theme(
      data: Theme.of(context).copyWith(splashColor: Colors.transparent),
      child: TextFormField(
          maxLines: maxLines,
          minLines: minLines,
          keyboardType: keyboardType,
          autofocus: autofocus,
          onChanged: onChanged,
          initialValue: initialValue,
          textInputAction: textInputAction,
          controller: controller,
          style: kTextStyleRegular,
          obscureText: password,
          textCapitalization: TextCapitalization.sentences,
          decoration: InputDecoration(
              hintStyle: kTextStyleHint,
              labelStyle: kTextStyleHeading,
              hintText: placeholder,
              contentPadding:
                  const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
              filled: true,
              fillColor: kPrimaryWhite,
              prefixIcon: leading,
              suffixIcon: trailing != null
                  ? GestureDetector(onTap: trailingTapped, child: trailing)
                  : null,
              errorBorder: circularBorder.copyWith(
                  borderSide: const BorderSide(color: Colors.red)),
              focusedBorder: circularBorder.copyWith(
                  borderSide: const BorderSide(color: kPrimaryWhite)),
              enabledBorder: circularBorder.copyWith(
                  borderSide: const BorderSide(color: kPrimaryWhite)))));
}
