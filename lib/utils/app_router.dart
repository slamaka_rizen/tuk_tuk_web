import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:tuk_tuk_web/pages/house_page/house_page.dart';

import '../pages/video_call_page/call_page.dart';
import '../pages/welcome_page/welcome_page.dart';

class AppRouter {
  static late final FluroRouter router;

  static var initial = "/";
  static var house = "/house/:id";

  static var video = "/video";

  static void configureRoutes(FluroRouter router) {
    router.notFoundHandler =
        Handler(handlerFunc: (context, params) => Container());

    router.define(initial,
        handler: Handler(
            handlerFunc:
                (BuildContext? context, Map<String, List<String>> params) =>
                    const WelcomePage()));

    router.define(house,
        handler: Handler(
            handlerFunc: (context, params) =>
                HousePage(id: params["id"]?[0] ?? "")));

    router.define(video,
        handler: Handler(handlerFunc: (context, params) => const CallPage()));
  }

  static void setup() {
    final router = FluroRouter();
    AppRouter.configureRoutes(router);
    AppRouter.router = router;
  }
}
